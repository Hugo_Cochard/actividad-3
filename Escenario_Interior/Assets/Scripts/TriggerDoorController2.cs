using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDoorController2 : MonoBehaviour
{
     [SerializeField] private Animator myDoor = null;

    [SerializeField] private bool openTrigger = false;
    [SerializeField] private bool closeTrigger = false;

    [SerializeField] private string doorOpen = "Door";
    [SerializeField] private string DoorClose = "DoorClose";

    private void OnTriggerenter(Collider other){
        if(other.CompareTag("Player")){
            if(openTrigger){
                myDoor.Play(doorOpen, 0, 0.0f);
                gameObject.SetActive(false);
            }

            else if(closeTrigger){
                myDoor.Play(DoorClose, 0, 0.0f);
                gameObject.SetActive(false);
            }
        }
    }
}
