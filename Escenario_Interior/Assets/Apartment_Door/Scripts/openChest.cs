using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;

public class openChest : MonoBehaviour
{
    float opening = 1.0f;

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.F)){
            for(int i = 0; i<51;i++){
            transform.Rotate(0, 0, opening);
            }
        }
        else if(Input.GetKeyDown(KeyCode.H)){
            for(int i = 0; i<51;i++){
            transform.Rotate(0, 0, -opening);
            }
        }
    }
}
